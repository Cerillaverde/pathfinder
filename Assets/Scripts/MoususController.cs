using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoususController : MonoBehaviour
{
    public void LoadInfo(EnemyInfo infoEnemy, Vector3 initialPosition)
    {
        transform.position = initialPosition;
        transform.localScale = infoEnemy.scale;
        GetComponent<SpriteRenderer>().color = infoEnemy.color;
        GetComponent<Rigidbody2D>().angularVelocity = infoEnemy.angularSpeed;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GetComponent<Poolable>().ReturnToPool();
    }
}
