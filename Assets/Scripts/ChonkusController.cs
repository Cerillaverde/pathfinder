using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChonkusController : MonoBehaviour
{
    private void OnMouseDown()
    {
        print("Chonkus mouse down");
        Destroy(gameObject);
    }

    private void OnMouseEnter()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
    }

    private void OnMouseExit()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
    }
}
