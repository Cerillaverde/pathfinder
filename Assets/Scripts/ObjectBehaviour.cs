using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBehaviour : MonoBehaviour
{
	public GameObject m_Instance;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CreateObjects());
    }

    private IEnumerator CreateObjects()
    {
        while(true)
        {
            GameObject instance = Instantiate(m_Instance);
            instance.transform.position = new Vector2(Random.Range(-10f,10f),Random.Range(-10f,10f));
            instance.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0f,1f),Random.Range(0f,1f),Random.Range(0f,1f));
            yield return new WaitForSeconds(.2f);
        }
    }
}
