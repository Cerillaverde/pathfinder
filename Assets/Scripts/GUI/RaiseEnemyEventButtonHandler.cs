using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaiseEnemyEventButtonHandler : MonoBehaviour
{
    [SerializeField]
    private EnemyInfo m_Info;
    [SerializeField]
    private GameEventEnemy m_GameEvent;

    public void RaiseEvent()
    {
        m_GameEvent.Raise(m_Info);
    }
}
