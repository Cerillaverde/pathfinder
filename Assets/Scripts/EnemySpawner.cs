using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Enemy;

    [SerializeField]
    private EnemyInfo[] m_EnemyInfos;

    [SerializeField]
    private Pool m_Pool;
    
    public void SpawnEnemy(EnemyInfo enemyInfo)
    {
        Vector3 spawnPosition = new Vector3(Random.Range(-10f, 10f), 5, 0);
        GameObject enemy = m_Pool.GetElement();
        if (enemy)
        {
            enemy.GetComponent<MoususController>().LoadInfo(enemyInfo, spawnPosition);
        }

    }

    public void ReceiveEvent2Float(float param0, float param1)
    {
        Debug.Log(param0 + " - " + param1);
    }

    public void ReceiveEvent3Float(float param0, float param1, float param2)
    {
        Debug.Log(param0 + " - " + param1 + " - " + param2);
    }

    public void ReceiveEvent4Float(float param0, float param1, float param2, float param3)
    {
        Debug.Log(param0 + " - " + param1 + " - " + param2 + " - " + param3);
    }
}
