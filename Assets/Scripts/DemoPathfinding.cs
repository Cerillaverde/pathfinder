using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Pathfinding))]
public class DemoPathfinding : MonoBehaviour
{
    [SerializeField]
    Tilemap m_Tilemap;
    [SerializeField]
    Color m_PathColor;

    Pathfinding m_Pathfinding;
    Vector3Int m_Origin;
    Vector3Int m_Destiny;
    List<Vector3Int> m_Cells;

    private void Awake()
    {
        m_Pathfinding = GetComponent<Pathfinding>();
        m_Cells = new List<Vector3Int>();
    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            m_Origin = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        }

        if (Input.GetMouseButtonDown(1))
        {
            m_Destiny = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));

            if (m_Pathfinding.IsWalkableTile(m_Origin) && m_Pathfinding.IsWalkableTile(m_Destiny))
            {
                //canviem color que �s cosa de l'exemple
                ChangeTileColor(Color.white);

                //cerquem el nou cam� en coordenades de casella
                m_Cells.Clear();
                m_Pathfinding.FindPath(m_Origin, m_Destiny, out m_Cells);

                //canviem color que �s cosa de l'exemple
                ChangeTileColor(m_PathColor);

                //transformem el cam� trobat a coordenades m�n
                List<Vector3> worldCoordinatesPath;
                m_Pathfinding.FromCellPathToWorldPath(m_Cells, out worldCoordinatesPath);
                foreach (Vector3 coordinates in worldCoordinatesPath)
                    print(coordinates);

                //es pot fer directament en coordenades m�n sense necessitar el tilemap
                //per enlloc. A vegades pot resultar-vos �til
                List<Vector3> worldCoordinatesPathDebug;
                m_Pathfinding.FindPathWorldSpace(m_Tilemap.CellToWorld(m_Origin),
                                                 m_Tilemap.CellToWorld(m_Destiny),
                                                 out worldCoordinatesPathDebug);

                //aix� no �s m�s que un comprovador que les caselles s�n els que s�n.
                if(worldCoordinatesPathDebug.Count == worldCoordinatesPath.Count)
                {
                    worldCoordinatesPath.ForEach(delegate (Vector3 coordinates)
                    {
                        if (!worldCoordinatesPathDebug.Contains(coordinates))
                            Debug.LogError("path not correct");
                    });
                }
                else
                {
                    Debug.LogError("path not correct");
                }
                
            }
        }
    }

    private void ChangeTileColor(Color color)
    {
        foreach(Vector3Int cell in m_Cells)
        {
            /*
             * El flag ens permet modificar nom�s aquest Tile i no tots, ja que per
             * defecte estan tots connectats entre ells i pintariem tots els del
             * mateix tipus.
             */        
            m_Tilemap.SetTileFlags(cell, TileFlags.None);
            m_Tilemap.SetColor(cell, color);
        }
    }
}
